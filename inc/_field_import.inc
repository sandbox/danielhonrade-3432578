<?php

// JSON object field value processing
function _import_val($node_imported, $field_name = '', $field_type = 'string') {

  /* TO DOs
    Field types, https://www.drupal.org/docs/drupal-apis/entity-api/fieldtypes-fieldwidgets-and-fieldformatters

    Valid plugin IDs for Drupal\Core\Field\FieldTypePluginManager are: 

    comment, 
    datetime, 
    file_uri, 
    file, 
    image, 
    link, 
    list_float, 
    list_string, 
    list_integer, 
    path, 
    text_with_summary, 
    text, 
    text_long, 
    integer, 
    uri, 
    entity_reference, 
    uuid, 
    float, 
    email, 
    timestamp, 
    string, 
    language, 
    created, 
    changed, 
    map, 
    string_long, 
    password, 
    decimal, 
    boolean 

  */
  $value = '';

  switch ($field_type) {
    case 'multi': if (isset($node_imported->$field_name) && !empty($node_imported->$field_name)) {
                    $value = $node_imported->$field_name; 
                  }
                  else {
                    $value = '';
                  }
                  break;
    case 'text':  if (isset($node_imported->$field_name) && !empty($node_imported->$field_name)) {
                    $value = trim($node_imported->$field_name[0]->value); 
                  }              
                  else {
                    $value = '';
                  }
                  break;
   case 'email':  if (isset($node_imported->$field_name) && !empty($node_imported->$field_name)) {
                    $value = trim($node_imported->$field_name[0]->email); 
                  }              
                  else {
                    $value = '';
                  }
                  break;              
    case 'date':  if (isset($node_imported->$field_name) && !empty($node_imported->$field_name)) {
                    $value = date('Y-m-d', strtotime($node_imported->$field_name[0]->value)); 
                  }
                  else {
                    $value = '';
                  }
                  break;
    case 'ref':   if (isset($node_imported->$field_name) && !empty($node_imported->$field_name)) {
                    $value = $node_imported->$field_name[0]->target_id; 
                  }
                  else {
                    $value = '';
                  }
                  break;
    case 'filepath':   
                  if (isset($node_imported->$field_name) && !empty($node_imported->$field_name) && isset($node_imported->$field_name[0]->url)) {

                    $url = $node_imported->$field_name[0]->url;
                    $value = stripcslashes($url);
                  }
                  else {
                    $value = '';
                  }
                  break;                     
    default:      if (isset($node_imported->$field_name) && !empty($node_imported->$field_name)) {
                    $value = trim($node_imported->$field_name[0]->value);
                  }
                  else {
                    $value = '';
                  }
                  break;
  }
  
  return is_array($value) ? $value: strip_tags($value);
}


