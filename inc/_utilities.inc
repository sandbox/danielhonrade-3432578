<?php

function _get_field_data_sync_vars($process = 'source') {

  $config = \Drupal::service('config.factory')->getEditable('variable_get_set.api');

  $field_data_sync_saved =  $config->get('field_data_sync');

  if (empty($field_data_sync_saved)) {

    $entityTypeManager = \Drupal::service('entity_type.manager');

    $contentTypes = $entityTypeManager->getStorage('node_type')->loadMultiple();
    
    // Initialize
    $field_data_sync = array();
    $field_data_sync[$process . '_settings'] = array();
    $field_data_sync[$process . '_settings']['contentType']   = array();
    
    if ($process == 'source') { // as source
      $field_data_sync[$process . '_settings']['source']     = 0;
      $field_data_sync[$process . '_settings']['source_dns'] = $_SERVER['HTTP_HOST'];
      $field_data_sync[$process . '_settings']['target_dns'] = '';
      $field_data_sync[$process . '_settings']['username']   = '';
      $field_data_sync[$process . '_settings']['password']   = '';
    }
    else {
      $field_data_sync[$process . '_settings']['target']      = 0;
    }

    foreach ($contentTypes as $contentType) {

      //\Drupal::logger('contentType')->notice($contentType->id());

      $content_type = $contentType->id();

      // Initialize
      $field_data_sync[$process . '_settings']['contentType'][$content_type] = array();
      $field_data_sync[$process . '_settings']['contentType'][$content_type]['fields'] = array();

      $getFieldDefinitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $content_type);

      foreach ($getFieldDefinitions as $key => $field) {

        if ($process == 'source') { // as source

          // Initialize
          $field_data_sync[$process . '_settings']['contentType'][$content_type][$content_type . '_fields'] = 0;

          if (str_contains($key, 'title') ||
              str_contains($key, 'field_')) {
            $fieldType = $field->getType();
            if (($fieldType == 'string') || ($fieldType == 'text') || ($fieldType == 'integer')) { // only text or integer fields can be used as unique id
                //\Drupal::logger('field')->notice($key);
                $field_data_sync[$process . '_settings']['contentType'][$content_type]['fields'][$key] = $key;
            }
          }
        }
        else { // as target

          // Initialize
          $field_data_sync[$process . '_settings']['contentType'][$content_type][$content_type . '_fields'] = 0;

          if (str_contains($key, 'title') ||
              str_contains($key, 'body') ||
              str_contains($key, 'field_')) {
                $field_data_sync[$process . '_settings']['contentType'][$content_type]['fields'][$key] = array();
                $field_data_sync[$process . '_settings']['contentType'][$content_type]['fields'][$key]['name'] = $key;
                $field_data_sync[$process . '_settings']['contentType'][$content_type]['fields'][$key]['value'] = 0;
          }
        }
      }
    }
    $field_data_sync_saved = $field_data_sync;

  }

  return $field_data_sync_saved[$process . '_settings'];
}


// Helper - check post global variables
function _post_check($string = '') {
  return isset($_POST[$string]) ? trim($_POST[$string]): '';
}




