<?php
namespace Drupal\field_data_sync\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class FieldDataSyncSettings extends FormBase {
 /**
  * {@inheritdoc}
  */
 public function getFormId() {
   return 'field_data_sync_settings';
 }

  /**
  * {@inheritdoc}
  */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['#attached']['library'][] = 'field_data_sync/styling';

    $source = _get_field_data_sync_vars('source');
    $target = _get_field_data_sync_vars('target');

    //$sources = json_encode($source);
    //$sources = json_decode($sources);
    //dsm($source);

    $form['introduction'] = array(
      '#markup' => '<p>Your site can be both source and target for other sites. You can perform multiple updates on your different Drupal websites by simply updating 1 website. Although this site can only update 1 site, you can configure your target site to update another site. Note: This module must be installed and configured in all sites for the "chain reaction" updates to occur.<p>',
    );

    $form['source_settings'] = array(
      '#type' => 'details',
      '#title' => t('Source settings'),
      '#description' => t('Enabling this as a source site will push changes to another site (target).'),
      '#open' => TRUE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
      '#prefix' => '<div class="columns">',
      '#attributes' => array('class' => array('column')),
    );

    $form['source_settings']['source'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable as source'),
      '#default_value' => $source['source'],
    );

    $form['source_settings']['target_site_info'] = array(
      '#type' => 'details',
      '#title' => t('Target site\'s info'),
      '#description' => t('This site as a source will be needing the target site (dependent) to update. Configure the target site\'s access.'),
      '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
    );

    $form['source_settings']['target_site_info']['target_dns'] = array(
      '#type' => 'textfield',
      '#title' => t('Target site\'s domain name'),
      '#attributes' => array('placeholder' => 'target_site.com'),
      '#description' => t('The domain name of the dependent site which will receive the updates coming from this site.'),
      '#default_value' => $source['target_dns'],
    );

    $form['source_settings']['target_site_info']['username'] = array(
      '#type' => 'textfield',
      '#title' => t('User name'),
      '#description' => t('This user must have field administration, node create/update permissions in the target site.'),
      '#default_value' => $source['username'],
    );

    $form['source_settings']['target_site_info']['password'] = array(
      '#type' => 'password',
      '#title' => t('Password'),
      '#default_value' => $source['password'],
    );

    $form['source_settings']['content_types'] = array(
      '#type' => 'details',
      '#title' => t('Content Types'),
      '#description' => t('Configure the dependent site unique fields for every content type, which will serve as temporary identifier for the dependent site\'s node. Once the updates are pushed to the dependent site, it will use the field data sync ID assigned to it.'),
      '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
    );

    foreach ($source['contentType'] as $ctype => $fields) {
      
      if ($ctype != $ctype . '_fields') {
        $form['source_settings']['content_types'][$ctype] = array(
          '#type' => 'fieldset',
          '#title' => t(strtoupper($ctype)),
          '#attributes' => array('id' => 'source-content-type-' . $ctype,  'class' => array('content-type')),
          '#collapsible' => FALSE,
          '#collapsed' => FALSE,
        );
        
        $options = array();
    
        if(!empty($fields['fields'])) {

          $getFieldDefinitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $ctype);

          $options = array();
          foreach ($fields['fields'] as $fkey => $field) {
            if (($fkey != 'field_fds_id') && ($fkey != 'field_fds_source')) {
              $options[$fkey] = $getFieldDefinitions[$fkey]->getLabel() . ' (' . $fkey . ')';
            }
          }

          $form['source_settings']['content_types'][$ctype][$ctype . '_fields'] = array(
            '#type' => 'radios',
            '#title' => t('Field/s'),
            '#options' => $options,
            '#default_value' => $source['contentType'][$ctype][$ctype . '_fields'],
          );
        }
        else {
          $form['source_settings']['content_types'][$ctype]['message'] = array(
            '#markup' => '<p>No qualified field available to serve as unique identifier for the node.<p>',
          );
        }
      }
    }

    $target = _get_field_data_sync_vars('target');

    $form['target_settings'] = array(
      '#type' => 'details',
      '#title' => t('Target settings'),
      '#description' => t('Enabling this as a target site will allow updates from another site (source).'),
      '#open' => TRUE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
      '#suffix' => '<div class="columns">',
      '#attributes' => array('class' => array('column')),
    );

    $form['target_settings']['target'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable as target'),
      '#default_value' => $target['target'],
    );

    $form['target_settings']['source_site_info'] = array(
      '#type' => 'details',
      '#title' => t('Source site\'s info'),
      '#description' => t('This site as a target will be getting updates from a source site. The current source site information will be displayed here once updated.'),
      '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
    );

    $form['target_settings']['source_site_info']['source_dns_info'] = array(
      '#type' => 'textfield',
      '#title' => t('Source site\'s domain name'),
      '#attributes' => array('placeholder' => 'source_site.com', 'disabled' => TRUE),
      '#description' => t('The domain name of the source site which the updates are coming from.'),
      '#default_value' => isset($target['source_dns']) ? $target['source_dns']: '',
    );

    $form['target_settings']['content_types'] = array(
      '#type' => 'details',
      '#title' => t('Content Types'),
      '#description' => t('Enable the fields for every content type to receive updates. Note: this will only work if the source site have similar content type/s and field/s.'),
      '#open' => FALSE, // Controls the HTML5 'open' attribute. Defaults to FALSE.
    );

    foreach ($target['contentType'] as $ctype => $fields) {

      $getFieldDefinitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $ctype);

      $form['target_settings']['content_types'][$ctype] = array(
        '#type' => 'fieldset',
        '#title' => t(strtoupper($ctype)),
        '#attributes' => array('id' => 'target-content-type-' . $ctype,  'class' => array('content-type')),
        '#collapsible' => FALSE,
        '#collapsed' => FALSE,
      );

      foreach ($fields['fields'] as $fkey => $field) {

        $form['target_settings']['content_types'][$ctype]['fields']['target_' . $fkey] = array(
          '#type' => 'checkbox',
          '#title' => t($getFieldDefinitions[$fkey]->getLabel() . ' (' . $field['name'] . ')'),
          '#attributes' => array('disabled' => (($fkey == 'field_fds_id') || ($fkey == 'field_fds_source')) ? TRUE : FALSE),
          '#default_value' => $target['contentType'][$ctype]['fields'][$fkey]['value'],
        );

      }
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $target_dns = $form_state->getValue('target_dns');

    if ($target_dns == $_SERVER['HTTP_HOST']) {
      $form_state->setErrorByName('target_dns', t('You cannot use ' . $_SERVER['HTTP_HOST'] . ' as your target site.'));
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = \Drupal::service('config.factory')->getEditable('variable_get_set.api');

    $source = _get_field_data_sync_vars('source');
    $target = _get_field_data_sync_vars('target');

    $field_data_sync_saved =  array();

    $entityTypeManager = \Drupal::service('entity_type.manager');

    $contentTypes = $entityTypeManager->getStorage('node_type')->loadMultiple();
    
    // Initialize
    $field_data_sync = array();
    $field_data_sync['source_settings'] = array();
    $field_data_sync['source_settings']['contentType'] = array();
    $field_data_sync['source_settings']['source']      = $form_state->getValue('source');
    $field_data_sync['source_settings']['source_dns']  = $_SERVER['HTTP_HOST']; // Current site's DNS
    $field_data_sync['source_settings']['target_dns']  = $form_state->getValue('target_dns');
    $field_data_sync['source_settings']['username']    = $form_state->getValue('username');
    $field_data_sync['source_settings']['password']    = !empty($form_state->getValue('password')) ? $form_state->getValue('password'): $source['password'];

    $field_data_sync['target_settings'] = array();
    $field_data_sync['target_settings']['contentType'] = array();
    $field_data_sync['target_settings']['target']      = $form_state->getValue('target');

    //dsm($form_state->getValue['source']);

    foreach ($contentTypes as $contentType) {

      //\Drupal::logger('contentType')->notice($contentType->id());

      $content_type = $contentType->id();

      // Initialize
      $fields = array();

      $field_data_sync['source_settings']['contentType'][$content_type] = array();
      $field_data_sync['source_settings']['contentType'][$content_type]['fields'] = array();
      $field_data_sync['target_settings']['contentType'][$content_type] = array();
      $field_data_sync['target_settings']['contentType'][$content_type]['fields'] = array();

      $getFieldDefinitions = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $content_type);

      foreach ($getFieldDefinitions as $key => $field) {
        
        $fields[] = $key;

        // Initialize
        if(str_contains($key, 'title') ||
           str_contains($key, 'field_')) {
 
            $fieldType = $field->getType();
            if (($fieldType == 'string') || ($fieldType == 'text') || ($fieldType == 'integer')) { // only text or integer fields can be used as unique id
                //\Drupal::logger('field')->notice($key);
                $field_data_sync['source_settings']['contentType'][$content_type][$content_type . '_fields'] = $form_state->getValue($content_type . '_fields');
                $field_data_sync['source_settings']['contentType'][$content_type]['fields'][$key] = $key;

            }
        }

        if (str_contains($key, 'title') ||
            str_contains($key, 'body') ||
            str_contains($key, 'field_')) {
              $field_data_sync['target_settings']['contentType'][$content_type]['fields'][$key] = array();
              $field_data_sync['target_settings']['contentType'][$content_type]['fields'][$key]['name'] = $key;
              $field_data_sync['target_settings']['contentType'][$content_type]['fields'][$key]['value'] = $form_state->getValue('target_' . $key);
        }
      }

      if ($form_state->getValue('target') == 1) {
        // Create FDS fields
        if (!in_array('field_fds_id', $fields)) {
          $this->_create_fds_fields($content_type, 'field_fds_id', 'Field Data Sync ID');
          // Add FDS ID to variable
          $field_data_sync['source_settings']['contentType'][$content_type]['fields']['field_fds_id'] = 'field_fds_id';
          $field_data_sync['target_settings']['contentType'][$content_type]['fields']['field_fds_id']['name'] = 'field_fds_id';
          $field_data_sync['target_settings']['contentType'][$content_type]['fields']['field_fds_id']['value'] = $form_state->getValue('target_field_fds_id');
        }

        if (!in_array('field_fds_source', $fields)) {
          $this->_create_fds_fields($content_type, 'field_fds_source', 'Field Data Sync Source');
          // Add FDS Source to variable
          $field_data_sync['source_settings']['contentType'][$content_type]['fields']['field_fds_source'] = 'field_fds_source';
          $field_data_sync['target_settings']['contentType'][$content_type]['fields']['field_fds_source']['name'] = 'field_fds_source';
          $field_data_sync['target_settings']['contentType'][$content_type]['fields']['field_fds_source']['value'] = $form_state->getValue('target_field_fds_source');

        }
      }
    }

    $field_data_sync_saved = $field_data_sync;

    $config->set('field_data_sync', $field_data_sync_saved);
    $config->save();
    
    if($config->save()){
      \Drupal::messenger()->addMessage("Field data sync configuration successfully updated.");
    }  
  }

  private function _create_fds_fields($content_type = NULL, $fkey = NULL, $flabel = NULL) {
    
    $query = \Drupal::database()->select('config', 'c')
      ->fields('c', array('name'))
      ->condition('c.name', 'field.storage.node.' . $fkey);
    $result = $query->execute()->fetchAssoc();

    $all_content_type_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $content_type);

    if (empty($result)) {
      // Create field storage.
      \Drupal\field\Entity\FieldStorageConfig::create(array(
        'field_name' => $fkey,
        'entity_type' => 'node',
        'type' => 'string',
        'cardinality' => 1,
      ))->save();    
    }
    if (!isset($all_content_type_fields[$fkey])) {

      // Attach an instance of the field to the page content type
      \Drupal\field\Entity\FieldConfig::create([
        'field_name' => $fkey,
        'entity_type' => 'node',
        'bundle' => $content_type,
        'label' => $flabel,
      ])->save();

      // Set Form Display
      \Drupal::service('entity_display.repository')->getFormDisplay('node', $content_type, 'default')->setComponent($fkey, array('type' => 'text_textfield'))->save();

      // Set Display
      \Drupal::service('entity_display.repository')->getViewDisplay('node', $content_type, 'default')->setComponent($fkey, array('type' => 'string'))->save();

      // Load the field
      $field_config = \Drupal\field\Entity\FieldStorageConfig::loadByName('node', $fkey);
    }
  }
}




