<?php

namespace Drupal\field_data_sync\Controller;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response; // Can accept plain text without the page markups returns

class FieldDataSync {
  
  // Export to dependent site
  public function Export($id, $type) {

    //\Drupal::logger('export')->notice(1);

    $output = '';

    $config = \Drupal::service('config.factory')->getEditable('variable_get_set.api');

    $field_data_sync_saved  = $config->get('field_data_sync');

    if (!empty($field_data_sync_saved) && ($field_data_sync_saved['source_settings']['source'] == 1)) {

      //\Drupal::logger('source_settings')->notice($field_data_sync_saved['source_settings']['source']);

      $node = \Drupal\node\Entity\Node::load($id);
      $data = \Drupal::service('serializer')->serialize($node, 'json');

      // Send to target site
      $url    = 'http://' . $field_data_sync_saved['source_settings']['target_dns'] . '/field_data_sync/import';                                   
      $data   = json_encode($data);
      $source = json_encode($field_data_sync_saved['source_settings']);
      
      $fields = "id=" . $id . "&type=" . $type . "&data=" . $data . "&source_settings=" . $source;

      if (!empty($id) && !empty($type) && !empty($data) && !empty($url)) {
         
        // CURL Request
        $username = $field_data_sync_saved['source_settings']['username'];
        $password = $field_data_sync_saved['source_settings']['password'];
        
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERPWD, $username . ':' . $password);
        curl_setopt($curl, CURLOPT_POST, 1);
        //curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $json_data);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);

        // Debug
        /*
          $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
          $responseInfo = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
          $errNo = curl_errno($curl);
          $errStr = curl_error($curl);
      
          print_r('- ' . $response . '<br />');
          print_r('- ' . $responseCode . '<br />');
          print_r('- ' . $responseInfo . '<br />');
          print_r('- ' . $errNo . '<br />');
          print_r('- ' . $errStr . '<br />');
        */
      
        $err = curl_error($curl);

        curl_close($curl);
      
        if ($err) {

          print "cURL Error #:" . $err;
        } else {
          if(strpos($_SERVER['REQUEST_URI'], 'batch') !== false) {
            // do nothing
          }
          else {
            // Response check
            if ($type == 'node') $output = 'Node was exported successfully.';
          }
        }
      }
    }
    // Show result of process data
    $response = new Response();
    $response->setContent($output);
    return $response;
  }
  

  // Node Import / Update
  public function Import($data = NULL) {
    
    $uid = \Drupal::currentUser()->id();

    //\Drupal::logger('import')->notice($uid);

    $output = '';

    $config = \Drupal::service('config.factory')->getEditable('variable_get_set.api');

    $field_data_sync_saved =  $config->get('field_data_sync');

    $target = $field_data_sync_saved['target_settings'];

    if (!empty($field_data_sync_saved) && ($field_data_sync_saved['target_settings']['target'] == 1)) {
      
      $id       = _post_check('id');
      $type     = _post_check('type');
      $data     = _post_check('data');
      $source   = _post_check('source_settings');
      
      if (!empty($id) && !empty($type) && !empty($data) && !empty($source)) {
        
        // Do the node import/parsing
        $data   = json_decode($data);
        $data   = json_decode($data);
        $source = json_decode($source, TRUE);

        //$node_imported = $data[0];
        $node_imported = $data;
        //\Drupal::logger('import')->notice($id);

        $source_nid = $node_imported->nid[0]->value;
        $content_type = $node_imported->type[0]->target_id;
            
        // FDS ID
        $query = \Drupal::database()->select('node_field_data', 'n')
          ->fields('n', array('nid', 'created', 'uid'))
          ->fields('x', array('entity_id', 'field_fds_id_value'))
          ->condition('n.type', $content_type)
          ->condition('x.field_fds_id_value', $source_nid)
          ->orderBy('n.created', 'DESC')
          ->range(0, 1);
          
        $query->join('node__field_fds_id','x','n.nid = x.entity_id');

        $result = $query->execute()->fetchAssoc();
        
        if (empty($result)) {
          $unique_field       = $source['contentType'][$content_type][$content_type . '_fields'];
          $unique_value = $node_imported->$unique_field[0]->value;

          if (!empty($unique_field)) {
            if ($unique_field == 'title') {
              // Get temporary unique title
              $query = \Drupal::database()->select('node_field_data', 'n')
                ->fields('n', array('nid', 'title', 'created', 'uid'))
                ->condition('n.type', $content_type)
                ->condition('n.title', $unique_value)
                ->orderBy('n.created', 'DESC')
                ->range(0, 1);
                
              $result = $query->execute()->fetchAssoc();
            }
            else {
              // Get temporary unique field ID
              $query = \Drupal::database()->select('node_field_data', 'n')
                ->fields('n', array('nid', 'created', 'uid'))
                ->fields('x', array('entity_id', $unique_field . '_value'))
                ->condition('n.type', $content_type)
                ->condition('x.' . $unique_field . '_value', $unique_value)
                ->orderBy('n.created', 'DESC')
                ->range(0, 1);
                
              $query->join('node__' . $unique_field,'x','n.nid = x.entity_id');

              $result = $query->execute()->fetchAssoc();
            }
          }
        } 

        // Updated existing node
        if (!empty($result)) {
          $node = \Drupal\node\Entity\Node::load($result['nid']);
        }
        else {      

          // Create new node
          $node = \Drupal\node\Entity\Node::create([
            'type' => $content_type, 
            'uid' => $uid,
            'status' => 1,
            'comment' => 0,
            'promote' => 0,
          ]);
        }
        
        if (empty($field_data_sync_saved['target_settings']['source_dns'])) {

          $field_data_sync_saved['target_settings']['source_dns'] = ($source['source_dns'] != $_SERVER['HTTP_HOST']) ? $source['source_dns']: '';
          $config->set('field_data_sync', $field_data_sync_saved);
          $config->save();

          if($config->save()){
            //\Drupal::messenger()->addMessage("Field data sync configuration successfully updated.");
          } 
        }



        $this->_node_import($node, $node_imported, $source, $target);

      }
    }

    // Show result of process data
    $response = new Response();
    $response->setContent($output);
    return $response;    
  }
  

  // Parent Company node import
  private function _node_import(&$node, $node_imported = NULL, $source = NULL, $target = NULL) {

    $changes = 0;
    $updated_fields = array();

    foreach ($target['contentType'] as $ctype => $fields) {

      if ($ctype == $node->getType()) {
        foreach ($fields['fields'] as $fkey => $field) {
          
          $fieldType = $node->get($fkey)->getFieldDefinition()->getType();

          if ($fkey == 'field_fds_id') {
            if (empty($node->field_fds_id->value)) {
              $node->set($fkey, $node_imported->nid[0]->value);
              $updated_fields[] = $fkey;
              $changes++;
            }
          }
          elseif ($fkey == 'field_fds_source') {
            if (empty($node->field_fds_source->value)) {
              $node->set($fkey, $source['source_dns']);
              $updated_fields[] = $fkey;
              $changes++;
            }
          }
          // Check if any changes in the data update
          elseif (!empty($node->$fkey->value) && isset($node_imported->$fkey[0]) && (strip_tags($node->$fkey->value) != strip_tags($node_imported->$fkey[0]->value)) && ($target['contentType'][$ctype]['fields'][$fkey]['value'] == 1)) {
            // Old node
            $node->set($fkey, _import_val($node_imported, $fkey, $fieldType));
            //\Drupal::logger('field old')->notice($fkey);
            $updated_fields[] = $fkey;
            $changes++; 
          }
          elseif ((empty($node->$fkey->value)) && ($target['contentType'][$ctype]['fields'][$fkey]['value'] == 1)) {
            // New node
            $node->set($fkey, _import_val($node_imported, $fkey, $fieldType));
            //\Drupal::logger('field new')->notice($fkey);
            $updated_fields[] = $fkey;
            $changes++;
          }
        }
      }
    }
    
    // Do not save if no new updates/changes
    if ($changes != 0) {
      $node->save();
      $updated_fields_text = '';

      foreach ($updated_fields as $key => $fieldname) {
        $updated_fields_text .= !empty($updated_fields_text) ? ', ': '';
        $updated_fields_text .= $fieldname;
      }

      $message = "Successfully updated field/s: \"" . $updated_fields_text . "\" from \"" . $source['source_dns'] . "\"";
      \Drupal::logger('FDS Updates')->notice($message);
    }
  }     
}








